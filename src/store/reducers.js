const INITIAL_STATE = {
  filter: {
    completed: null,
    text: ''
  },
  todos: []
};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {

    case 'FETCH_TODOS': {
      const { todos } = action.payload;

      return {
        ...state,
        todos
      };
    }

    case 'SET_FILTER_COMPLETED': {
      const { completed } = action.payload;

      return {
        ...state,
        filter: {
          ...state.filter,
          completed
        }
      };
    }

    case 'SET_FILTER_TEXT': {
      const { text } = action.payload;

      return {
        ...state,
        filter: {
          ...state.filter,
          text
        }
      };
    }

    default: {
      return state;
    }

  };

};
