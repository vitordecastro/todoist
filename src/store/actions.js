export const fetchTodos = (todos) => {
  return {
    type: 'FETCH_TODOS',
    payload: { todos }
  };
};

export const setFilterCompleted = (completed) => {
  return {
    type: 'SET_FILTER_COMPLETED',
    payload: { completed }
  };
};

export const setFilterText = (text) => {
  return {
    type: 'SET_FILTER_TEXT',
    payload: { text }
  };
};
