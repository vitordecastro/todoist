import React from 'react';
import { connect } from 'react-redux';

import TodosIndexList from './TodosIndexList';
import TodosIndexToolbar from './TodosIndexToolbar';
import TodosForm from './TodosForm';

import api from './services/api';
import {
  fetchTodos,
  setFilterCompleted,
  setFilterText
} from './store/actions';

class TodosIndex extends React.Component {

  constructor (props) {
    super(props);

    this.fetchTodos();
  }

  componentDidUpdate (props) {
    if (this.props.filter !== props.filter) {
      this.fetchTodos();
    }
  }

  fetchTodos = async () => {
    const { dispatch, filter } = this.props;

    const todos = await api.fetchTodos(filter);

    dispatch(fetchTodos(todos));
  }

  setFilterCompleted = (completed) => {
    const { dispatch } = this.props;

    dispatch(setFilterCompleted(completed));
  }

  setFilterText = (text) => {
    const { dispatch } = this.props;

    dispatch(setFilterText(text));
  }

  createTodo = async (todo) => {
    const new_todo = await api.createTodo(todo);
  }

  onSubmit = (form) => {
    this.createTodo(form);
  }

  render () {
    const { filter, todos } = this.props;

    return (
      <div>
        <TodosIndexList
          todos={todos}
        />
        <TodosIndexToolbar
          filter={filter}
          setFilterCompleted={this.setFilterCompleted}
          setFilterText={this.setFilterText}
        />
        <TodosForm
          onSubmit={this.onSubmit}
        />
      </div>
    );
  }

};

const mapStateToProps = (state) => ({
  filter: state.filter,
  todos: state.todos
});

export default connect(mapStateToProps)(TodosIndex);
