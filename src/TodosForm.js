import React from 'react';
import api from './services/api';

class TodosForm extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      form: {
        short_desc: '',
        desc: ''
      }
    };
  }

  setFormFieldValue = (e) => {
    this.setState({
      ...this.state,
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  }

  render() {
    const { onSubmit } = this.props;
    const { form } = this.state;

    return (
      <div>
        <form>
          <br/>
          <label>
            Short Description:
          </label>
          <br/>
          <input
              type="text"
              name="short_desc"
              value={form.short_desc}
              onChange={this.setFormFieldValue}
            />
          <br/>

          <br/>
          <label>
            Description:
          </label>
          <br/>
          <input
              type="text"
              name="desc"
              value={form.desc}
              onChange={this.setFormFieldValue}
            />
          <br/>

          <br/>
          <input type="submit" onClick={() => onSubmit(this.state.form)} />
        </form>
      </div>
    );
  }

}

export default TodosForm;