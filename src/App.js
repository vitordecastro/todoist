import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import TodosIndex from './TodosIndex';

import reducers from './store/reducers';
const store = createStore(reducers);

class App extends React.Component {

  render () {
    return (
      <Provider store={store}>
        <TodosIndex />
      </Provider>
    );
  }

};

export default App;
