import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:3001',
  timeout: 1000
});

api.interceptors.response.use((response) => response.data);

export default {

  fetchTodos: async (params) => api.get('/todos.json', { params }),

  createTodo: async (todo) => api.post('/todos.json', { todo })

}

// export default {

//   fetchTodos: async (filter) => {
//     // TODO: Implementar requisição para API
//     let todos = [
//       {
//         id: 1,
//         short_desc: 'Todo 1',
//         desc: 'Todo 1 description',
//         completed: 1
//       },
//       {
//         id: 2,
//         short_desc: 'Todo 2',
//         desc: 'Todo 2 description',
//         completed: 0
//       },
//       {
//         id: 3,
//         short_desc: 'Todo 3',
//         desc: 'Todo 3 description',
//         completed: 0
//       }
//     ];

//     if (filter.completed != null) {
//       todos = todos.filter((todo) => todo.completed === filter.completed);
//     }
//     if (filter.text != null && filter.text != '') {
//       todos = todos.filter((todo) => todo.short_desc.includes(filter.text));
//     }

//     return todos;
//   }

// };
