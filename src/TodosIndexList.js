import React from 'react';

class TodosIndexList extends React.PureComponent {

  render () {
    const { todos } = this.props;

    return (
      <ul>
        {todos.map((todo) => (
          <li key={todo.id}>
            {todo.short_desc}
          </li>
        ))}
      </ul>
    );
  }

};

export default TodosIndexList;
