import React from 'react';
import { thisExpression } from '@babel/types';

class TodosIndexToolbar extends React.PureComponent {

  constructor (props) {
    super (props);
    this.state = { filterText: '' };
  }
  completedButtonColor = (completed) => {
    const { filter } = this.props;

    return filter.completed === completed ? '#CCC' : '#FFF';
  }

  setFilterTextInput = (e) => {
    const filterText = e.target.value;
    this.setState({ ...this.state, filterText });
  }

  render () {
    const {
      setFilterCompleted,
      setFilterText
    } = this.props;

    return (
      <div>
        <div>
          <button
            onClick={() => setFilterCompleted(null)}
            style={{
              backgroundColor: this.completedButtonColor(null)
            }}
          >
            Todos
          </button>
          <button
            onClick={() => setFilterCompleted(0)}
            style={{
              backgroundColor: this.completedButtonColor(0)
            }}
          >
            Ativos
          </button>
          <button
            onClick={() => setFilterCompleted(1)}
            style={{
              backgroundColor: this.completedButtonColor(1)
            }}
          >
            Concluídos
          </button>
        </div>
        <div>
          <input
            type='text'
            value={this.state.filterText}
            onChange={this.setFilterTextInput}
          />
          <button
            onClick={() => {
              setFilterText(this.state.filterText);
            }}>
            Search
          </button>
        </div>
      </div>
    );
  }

};

export default TodosIndexToolbar;
